<?php
class polls_box extends boxes_box {
  public function options_defaults() {
    return array(
      'surveys' => 0
    );
  }
  public function options_form() {
    $form = polls_select();
    $form['surveys']['#default_value'] = $this->surveys;
    return $form;
  }
  public function render() {
    $block = array('delta' => $this->delta);
    $survey = node_load($this->options['surveys']);
    if ($survey) {
      $block['content'] = drupal_get_form('polls_issue', $survey);
      $block['title'] = check_plain($survey->title);
      $block['subject'] = check_plain($survey->title);
    }
    else {
      $block['content'] = t('survey not found');
      $block['title'] = t('survey not found');
      $block['subject'] = t('survey not found');
    }
    return $block;
  }
}
